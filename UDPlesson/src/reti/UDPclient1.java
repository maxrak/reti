package reti;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPclient1 {
	static int clientPort=1234;
	static int serverPort=4321;
	public static void main(String args[]) throws Exception {
		
		//Buffer per leggere i dati da console dall'utente.
		BufferedReader inFromUser=new BufferedReader(new InputStreamReader(System.in));
		
		//Indirizzo interenet a cui mandare i dati, selezionato per nome (utilizzo del DNS). 
		//L'esercizio viene eseguito in locale e quindi il server si trova sulla stessa macchina
		InetAddress IPAddress=InetAddress.getByName("localhost");
		
		//Buffer in cui memorizzare i dati da leggere e scrivere.
		byte[] sendData =new byte[1024];
		
		System.out.println("Inserici il messaggio da spedire:");
		//Lettura dei dati dell'utente
		String frase=inFromUser.readLine();
		
		//Inseriamo i dati nel buffer -> notare la conersione in byte.
		sendData=frase.getBytes();
		
		//Creiamo una socket sulla porta prefissata
		DatagramSocket clientSocket=new DatagramSocket(clientPort);
		
		System.out.println("Mex:"+sendData.toString());
		//Creiamo un Datagram 
		DatagramPacket datagram=new DatagramPacket(sendData, sendData.length, IPAddress, serverPort);
		
		System.out.println("Spedisco il messaggio di lunghezza:"+sendData.length+" a"+IPAddress.getCanonicalHostName()+":"+serverPort);
		//Spediamo il datagram
		clientSocket.send(datagram);
		System.out.println("Spedito");
		clientSocket.close();
	}
}
