package reti;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPgateway {
	static int clientPort=1234;
	static int serverPort=4321;
	static int gw=3333;
	
	public static void main(String args[]) throws Exception {
		
		//Buffer in cui memorizzare i dati da leggere e scrivere.
		byte[] receivedData =new byte[1024];
		byte[] sendData =new byte[1024];

		//Creiamo una socket sulla porta prefissata
		DatagramSocket gwSocket=new DatagramSocket(gw);

		//Creiamo la struttura dati per ospitare il datagram da ricever
		DatagramPacket datagram=new DatagramPacket(receivedData, receivedData.length);
		
		System.out.println("In Ricezione");
		//Riceviamo (ci mettiamo in attesa di) un Datagramma
		gwSocket.receive(datagram);
		System.out.println("Ricevuto");	

		//Recuperiamo i dati dal datagram
		String  frase= new String(datagram.getData());

		//Stampiamo il risultato
		System.out.println("Messaggio sulla socket dalla porta"+datagram.getPort());
		
		System.out.println("Mex:"+frase);

		//Creiamo un Datagram 
		datagram=new DatagramPacket(sendData, sendData.length, datagram.getAddress(), serverPort);
		
		System.out.println("Spedisco il messaggio di lunghezza:"+sendData.length+" a "+datagram.getAddress().getCanonicalHostName()+":"+datagram.getPort());
		
		//Spediamo il datagram
		gwSocket.send(datagram);
		System.out.println("Spedito");
		gwSocket.close();

		
		System.out.println("Rispedisco sulla socket alla porta"+datagram.getPort());
		//Chiudiamo la socket
		gwSocket.close();
	}
}
