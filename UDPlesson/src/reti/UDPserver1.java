package reti;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPserver1 {
	static int clientPort=1234;
	static int serverPort=4321;
	
	public static void main(String args[]) throws Exception {
		
		//Buffer in cui memorizzare i dati da leggere e scrivere.
		byte[] receivedData =new byte[1024];

		//Creiamo una socket sulla porta prefissata
		DatagramSocket serverSocket=new DatagramSocket(serverPort);

		//Creiamo la struttura dati per ospitare il datagram da ricever
		DatagramPacket datagram=new DatagramPacket(receivedData, receivedData.length);
		
		System.out.println("In Ricezione");
		//Riceviamo (ci mettiamo in attesa di) un Datagramma
		serverSocket.receive(datagram);
		System.out.println("Ricevuto");	

		//Recuperiamo i dati dal datagram
		String  frase= new String(datagram.getData());
		
		//Stampiamo il risultato
		System.out.println("Messaggio:"+frase+" sulla socket dalla porta"+datagram.getPort());
		
		//Chiudiamo la socket
		serverSocket.close();
		
	}
}
