package reti;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPserver3 {
	int clientPort = 1234;
	static int serverPort = 4321;

	public static void main(String args[]) throws Exception {

		//Buffer per leggere i dati da console dall'utente.
		BufferedReader inFromUser=new BufferedReader(new InputStreamReader(System.in));

		//Buffer in cui memorizzare i dati da leggere e scrivere.
		byte[] receivedData =new byte[1024];

		System.out.println("Inserici il numero di porta su cui ricevere:");
		//Lettura dei dati dell'utente
		String port=inFromUser.readLine();
		serverPort=Integer.parseInt(port);
		if (serverPort>65536) { 
			System.out.println("Numero di porta non valido");
			System.exit(-1);
		}

		//Creiamo una socket sulla porta prefissata
		DatagramSocket serverSocket=new DatagramSocket(serverPort);

		//Creiamo la struttura dati per ospitare il datagram da ricever
		DatagramPacket datagram=new DatagramPacket(receivedData, receivedData.length);

		boolean receive=true;

		while(receive){
			System.out.println("In Ricezione");
			//Riceviamo (ci mettiamo in attesa di) un Datagramma
			serverSocket.receive(datagram);
			System.out.println("Ricevuto");	

			//Recuperiamo i dati dal datagram
			String  frase= new String(datagram.getData());

			//Stampiamo il risultato
			System.out.println("Messaggio sulla socket dalla porta:"+datagram.getPort());
			if(frase.trim().equals("close")) {
					System.out.println("Mex:"+frase+"Chiudo il server");
					receive=false;
				} else {
					System.out.println("Mex:"+frase);
				};
		}
		//Chiudiamo la socket
		serverSocket.close();

	}
}
